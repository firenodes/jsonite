import ky from "ky";

export class TransformationBuilder {
    protected transforms: ((str: string) => string)[];

    constructor(public readonly property: RegExp) {}

    titleCase() {
        this.transforms.push((str) =>
            str
                .split(" ")
                .map(
                    (word) =>
                        word[0].toUpperCase() + word.slice(1).toLowerCase()
                )
                .join(" ")
        );
    }

    upperCase() {
        this.transforms.push((str) => str.toUpperCase());
    }

    lowerCase() {
        this.transforms.push((str) => str.toLowerCase());
    }

    build() {
        return [...this.transforms];
    }
}

export class JsoniteClient {
    protected url: URL;
    protected transformations: TransformationBuilder[];

    constructor(public baseUrl: string, public apiKey: string) {}

    where(query?: Record<string, unknown>) {
        this.url.pathname = "/find";
        Object.entries(query).forEach((q) =>
            this.url.searchParams.append(q[0], q[1].toString())
        );
        return this;
    }

    from(collection: string) {
        this.url = new URL(`${this.baseUrl}/${collection}`);
        return this;
    }

    /*     transform(
        prop: string | RegExp,
        callback: (tb: TransformationBuilder) => TransformationBuilder
    ) {
        const property = new RegExp(prop);

        this.transformations.push(
            callback(new TransformationBuilder(property))
        );
    } */

    async fetch<T = Record<string, never>>(): Promise<T[]> {
        this.url.searchParams.append("APIKEY", this.apiKey);
        return ky(this.url).json();
    }

    toString() {
        return this.url;
    }
}

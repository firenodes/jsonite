# Jsonite 🔥

A simple database 🗃️ with a rest api built in 🕸️.

## Introduction

Jsonite currently uses **yaml** files as collections, making it easy to update the data by hand. This may change to a more efficient database such as Postgres in the future.

## Future Plans 📆

I plan on adding more features to the api such as deletion and insertion of entries. I also want to add a web interface where you can manage all your databases with an easy-to-use online editor (with markdown support 📝).

## Documentation 📄

Documentation is in a work-in-progress state, but it will be released soon.

## Issues 🚑

If you have any issues, [feel free to let me know about them](https://gitlab.com/firenodes/jsonite/-/issues).

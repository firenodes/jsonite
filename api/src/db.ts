import { readFile, WriteConfig } from "@theoparis/config";
import { Query } from "mingo";

export type DefaultDatabaseType = Record<string, unknown[]>;

export class Database<T = DefaultDatabaseType> {
    constructor(public readonly path: string) {}

    /**
     * Creates a collection instance.
     * @param key The collection name
     * @returns The newly-created collection instance
     */
    col<C = unknown>(key: string) {
        const cfg = readFile<T>(`${this.path}/${key}.yaml`, { type: "yaml" });
        return new Collection<C, T>(cfg, key);
    }
}

export class Collection<C = unknown, T = DefaultDatabaseType> {
    constructor(
        public readonly cfg: WriteConfig<T>,
        public readonly name: string
    ) {}

    protected obj(): C[] {
        this.cfg.read();
        return this.cfg.toObject()["data"];
    }

    /**
     * Takes in a query from https://npmjs.org/package/mingo,
     * and returns a mingo cursor object.
     */
    find(query: Query) {
        const obj = this.obj();
        return query.find(obj);
    }

    findOne(query: Query) {
        return this.find(query).all()[0];
    }
}

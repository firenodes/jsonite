import * as mingo from "mingo";
import { App } from "@tinyhttp/app";
import { Database } from "./db";
import { cors } from "@tinyhttp/cors";
import { logger } from "@tinyhttp/logger";

export class Application {
    readonly port: number;
    readonly app: App;
    readonly dataPath: string;
    readonly db: Database;
    readonly apiKey: string;

    constructor() {
        this.app = new App();
        this.port = parseInt(process.env.PORT || "6969");
        this.dataPath = process.env.DATA_PATH || "data";

        this.db = new Database(this.dataPath);

        this.apiKey = process.env.API_KEY || "1234";

        // Collection find query
        this.app.get("/:col/find", async (req, res) => {
            console.log(req.query.APIKEY, this.apiKey);
            if (this.apiKey && req.query.APIKEY !== this.apiKey)
                return res.status(401).json({
                    error: "Invalid api key in query parameter.",
                });
            if (!req.params.col || req.params.col === "")
                return res.status(400).json({
                    error: "Missing request parameter: 'col'. ",
                });
            const colId = req.params.col;
            const query = req.query;
            delete query.APIKEY;
            const data = await this.db
                .col(colId)
                .find(new mingo.Query(query))
                .all();
            res.json(data);
        });
    }

    useDefaultMiddleware() {
        this.app.use(logger(), cors());
    }

    useDefaultRoutes() {
        this.app.get("/", async (_req, res) =>
            res.json({
                message: "Hello, world! This is a jsonite api.",
            })
        );
    }

    /**
     * Use the default routing and middleware handlers for the app.
     */
    useDefaults() {
        this.useDefaultMiddleware();
        this.useDefaultRoutes();
        return this;
    }

    /**
     * Starts the application.
     */
    async start() {
        return this.app.listen(this.port, () =>
            console.log(`Listening on :${this.port}`)
        );
    }
}

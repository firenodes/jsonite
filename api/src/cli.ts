import { Application } from "./app";

new Application()
    .useDefaults()
    .start()
    .catch((err) => {
        console.error(err);
        process.exit(1);
    });
